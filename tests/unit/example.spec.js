import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import CouponCard from '@/components/CouponCard.vue';


describe('CouponCard.vue', () => {
  it('Render food name correctly', () => {
    const wrapper = shallowMount(CouponCard, {
      propsData: {
        food_name: 'A', shop_name: 'A Shop', price: 19, rareness: 'C', food_image: 'doge.jpg',
      },
    });
    expect(wrapper.text()).to.include('Teriyaki');
  });
});
