import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/CustomerHome.vue'),
    },
    {
      path: '/admin',
      name: 'admin',
      component: () => import('./views/AdminSignin.vue'),
    },
    {
      path: '/customer-registration',
      name: 'customer-registration',
      component: () => import('./views/CustomerRegistration.vue'),
    },
    {
      path: '/customer-payment',
      name: 'customer-payment',
      component: () => import('./views/CustomerPayment.vue'),
    },
    {
      path: '/search-result',
      name: 'search-result',
      component: () => import('./views/CustomerSearch.vue'),
    },
    {
      path: '/gashapon-animation',
      name: 'gashapon-animation',
      component: () => import('./components/AnimationPage.vue'),
    },
    {
      path: '/customer-coupon',
      name: 'customer-coupon',
      component: () => import('./views/CustomerCoupon.vue'),
    },
    {
      path: '/shop',
      name: 'shop',
      component: () => import('./views/ShopHome.vue'),
    },
    {
      path: '/shop-registration',
      name: 'shop-registration',
      component: () => import('./views/ShopRegistration.vue'),
    },
    {
      path: '/shop-coupon',
      name: 'shop-coupon',
      component: () => import('./views/ShopCoupon.vue'),
    },
    {
      path: '/shop-profile',
      name: 'shop-profile',
      component: () => import('./views/ShopProfile.vue'),
    },
    {
      path: '/admin-coupon',
      name: 'admin-coupon',
      component: () => import('./views/AdminCoupon.vue'),
    },
    {
      path: '*',
      redirect: '/',
    },
    {
      path: '/admin-form',
      name: 'admin-form',
      component: () => import('./components/AdminCouponForm.vue'),
    },
    {
      path: '/shop-use/:coupon_id/:username',
      name: 'use',
      component: () => import('./views/ConfirmUsed.vue'),
    },
    {
      path: '/admin-payment',
      name: 'admin-payment',
      component: () => import('./views/AdminPayment.vue'),
    },
  ],
});
