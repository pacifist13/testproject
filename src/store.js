/* eslint-disable */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    gashaFilter: {},
    gashaResult: [],
    loginStatus: false,
    loginInfo: {},
    gashaponID: -1,
    cookies: 0,
    drawCoupon: {},
  },
  mutations: {
    changeGashaResult(state, newList) {
      state.gashaResult = newList;
    },
    changeLoginStatus(state, newStatus) {
      state.loginStatus = newStatus;
    },
    changeGashaFilter(state, newFilter) {
      state.gashaFilter = newFilter;
    },
    changeLoginInfo(state, newFilter) {
      state.loginInfo = newFilter;
    },
    changeGashaponID(state, newGashaponID) {
      state.gashaponID = newGashaponID;
    },
    changeCookies(state, newCookies) {
      state.cookies = newCookies;
    },
    changeDrawCoupon(state, newCoupon) {
      state.drawCoupon = newCoupon;
    }
  },
  actions: {
    changeGashaResult(context, newList) {
      context.commit('changeGashaResult', newList);
    },
    changeLoginStatus(context, newStatus) {
      context.commit('changeLoginStatus', newStatus);
    },
    changeGashaFilter(context, newStatus) {
      context.commit('changeGashaFilter', newStatus)
    },
    changeLoginInfo(context, newStatus) {
      context.commit('changeLoginInfo', newStatus);
    },
    changeGashaponID(context, newID) {
      context.commit('changeGashaponID', newID);
    },
    changeCookies(context, newCookies) {
      context.commit('changeCookies', newCookies);
    },
    changeDrawCoupon(context, newCoupon) {
      context.commit('changeDrawCoupon', newCoupon);
    }
  },
  getters: {
    getGashaResult: (state) => {
      return state.gashaResult;
    },
    getLoginStatus: (state) => {
      return state.loginStatus;
    },
    getGashaFilter: (state) => {
      return state.gashaFilter;
    },
    getLoginInfo: (state) => {
      return state.loginInfo;
    },
    getGashaponID: (state) => {
      return state.gashaponID;
    },
    getCookies: (state) => {
      return state.cookies;
    },
    getDrawCoupon: (state) => {
      return state.drawCoupon;
    }
  },
});
