import Api from '@/services/Api';

export default {
  login(paramsObject) {
    return Api().post('admin/login', paramsObject);
  },
  logout() {
    return Api().post('logout');
  },
  getFilters() {
    return Api().get('filter');
  },
  getAllCouponReq() {
    return Api().get('admin/couponReq/all');
  },
  getTop3Gasha() {
    return Api().get('gashapon/top');
  },
  getAllGasha() {
    return Api().get('gashapon/search');
  },
  setTop3(paramsObject) {
    return Api().put('gashapon/top', paramsObject);
  },
  search(filters) {
    return Api().get('gashapon/search',
      {
        params: filters,
      });
  },
  getGashaponCoupon(paramsObject) {
    return Api().get('couponReq/rate', {
      params: paramsObject,
    });
  },
  getPaymentList() {
    return Api().get('payment');
  },
  paidToShop(paramsObject) {
    return Api().post('paidToshop', paramsObject);
  },
  getCouponReq(id) {
    return Api().get('admin/couponReq', {
      params: {
        req_id: id,
      },
    });
  },
  acceptCouponReq(paramsObject) {
    Api().post('admin/couponReq/accept', paramsObject);
  },
  editCouponReq(paramsObject) {
    Api().post('admin/couponReq/edit', paramsObject);
  },
  rejectCouponReq(paramsObject) {
    Api().post('admin/couponReq/reject', paramsObject);
  },
  autoClear() {
    Api().post('admin/autoClear');
  },
};
