import axios from 'axios';

export default () => axios.create({
  baseURL: 'http://13.250.46.0/api/',
  withCredentials: true,
  timeout: 2500,
});
