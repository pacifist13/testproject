import Api from '@/services/Api';

export default {
  login(paramsObject) {
    return Api().post('customer/login', paramsObject);
  },
  logout() {
    return Api().post('logout');
  },
  getInfo() {
    return Api().get('customer/info');
  },
  register(paramsObject) {
    return Api().post('customer/register', paramsObject);
  },
  getFilters() {
    return Api().get('filter');
  },
  search(filters) {
    const paramsObject = {};
    if (filters.location && (filters.location !== 'โซนร้านอาหารทั้งหมด')) {
      paramsObject.location = filters.location;
    }
    if (filters.price && (filters.price !== 'ราคาทั้งหมด')) {
      paramsObject.price = filters.price;
    }
    if (filters.type && (filters.type !== 'ประเภทอาหารทั้งหมด')) {
      paramsObject.type = filters.type;
    }
    return Api().get('gashapon/search',
      {
        params: paramsObject,
      });
  },
  getGashaponCoupon(paramsObject) {
    return Api().get('gashapon/coupon', {
      params: paramsObject,
    });
  },
  getCoupon() {
    return Api().get('customer/coupon');
  },
  topUp(paramsObject) {
    return Api().post('topUp', paramsObject);
  },
  drawCoupon(paramsObject) {
    return Api().get('gashapon/draw', {
      params: paramsObject,
    });
  },
  acceptCoupon(paramsObject) {
    return Api().post('coupon', paramsObject);
  },
  getCookies() {
    return Api().get('cookies');
  },
  cancelDraw(paramsObject) {
    return Api().post('gashapon/draw/cancel', paramsObject);
  },
  confirmCoupon(paramsObject) {
    return Api().post('gashapon/draw/confirm', paramsObject);
  },
  changeProfile(paramsObject) {
    return Api().put('customer/info', paramsObject);
  },
  getTop3() {
    return Api().get('gashapon/top');
  },
  canDraw(paramsObject) {
    return Api().get('gashapon/canDraw', {
      params: paramsObject,
    });
  },
};
