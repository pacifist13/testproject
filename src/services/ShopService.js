import Api from '@/services/Api';

export default {
  login(paramsObject) {
    return Api().post('shop/login', paramsObject);
  },
  getUserInfo() {
    return Api().get('shop/info');
  },
  logout() {
    return Api().post('logout');
  },
  register(paramsObject) {
    return Api().post('shop/register', paramsObject);
  },
  getAllCouponRequest() {
    return Api().get('couponReq');
  },
  submitNewCoupon(paramsObject) {
    return Api().post('couponReq', paramsObject);
  },
  editCoupon(paramsObject) {
    return Api().put('couponReq', paramsObject);
  },
  getUserMoney() {
    return Api().get('money');
  },
  getShopLocation() {
    return Api().get('shop/location');
  },
  changeProfile(paramsObject) {
    return Api().put('shop/info', paramsObject);
  },
  cancelCoupon(paramsObject) {
    return Api().post('couponReq/cancel', paramsObject);
  },
  getShopBank() {
    return Api().get('shop/location');
  },
  getCouponForUse(paramsObject) {
    return Api().get('coupon', {
      params: paramsObject,
    });
  },
  useCoupon(paramsObject) {
    return Api().delete('coupon', {
      params: paramsObject,
    });
  },
};
